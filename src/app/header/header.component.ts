import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  scrollTo(id: string) {
    let el = document.getElementById(id);
    if(el != null) {
      el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
  }
}
