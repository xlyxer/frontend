import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransparentButtonComponent } from './transparent-button/transparent-button.component';



@NgModule({
  declarations: [
    TransparentButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TransparentButtonComponent
  ]
})
export class ElementsModule { }
