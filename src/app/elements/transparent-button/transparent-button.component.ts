import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-transparent-button',
  templateUrl: './transparent-button.component.html',
  styleUrls: ['./transparent-button.component.css']
})
export class TransparentButtonComponent implements OnInit {

  @Input()
  text: string;

  constructor() { }

  ngOnInit(): void {
    this.text = this.text.toLowerCase();
  }

}
