import { NgModule } from '@angular/core';
import { ContentComponent } from "./content.component";
import { PromoComponent } from "./promo/promo.component";
import { AboutUsComponent } from './about-us/about-us.component';
import { TripPriceComponent } from './trip-price/trip-price.component';
import { FormEvaluateComponent } from './trip-price/form-evaluate/form-evaluate.component';
import { SelectComponent } from './trip-price/form-evaluate/select/select.component';
import { BlackButtonComponent } from './trip-price/form-evaluate/black-button/black-button.component';
import { PriceTableComponent } from './trip-price/price-table/price-table.component';
import { GetPromoComponent } from './get-promo/get-promo.component';
import { DeepRatingComponent } from './deep-rating/deep-rating.component';

import { BrowserModule } from '@angular/platform-browser';
import { ElementsModule } from '../elements/elements.module';


@NgModule({
  declarations: [
    ContentComponent,
    PromoComponent,
    AboutUsComponent,
    TripPriceComponent,
    FormEvaluateComponent,
    SelectComponent,
    BlackButtonComponent,
    PriceTableComponent,
    GetPromoComponent,
    DeepRatingComponent
  ],
  imports: [
    BrowserModule,
    ElementsModule
  ],
  exports: [
    ContentComponent,
    SelectComponent
  ],
})
export class ContentModule { }
