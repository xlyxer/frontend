import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetPromoComponent } from './get-promo.component';

describe('GetPromoComponent', () => {
  let component: GetPromoComponent;
  let fixture: ComponentFixture<GetPromoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetPromoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
