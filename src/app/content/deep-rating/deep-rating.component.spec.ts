import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeepRatingComponent } from './deep-rating.component';

describe('DeepRatingComponent', () => {
  let component: DeepRatingComponent;
  let fixture: ComponentFixture<DeepRatingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeepRatingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeepRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
