import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEvaluateComponent } from './form-evaluate.component';

describe('FormEvaluateComponent', () => {
  let component: FormEvaluateComponent;
  let fixture: ComponentFixture<FormEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
