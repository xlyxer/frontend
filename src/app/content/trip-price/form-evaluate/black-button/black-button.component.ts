import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-black-button',
  templateUrl: './black-button.component.html',
  styleUrls: ['./black-button.component.css']
})
export class BlackButtonComponent implements OnInit {

  @Input() text: string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
