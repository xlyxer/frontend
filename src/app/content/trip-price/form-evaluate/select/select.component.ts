import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  
  @Input() type: string;
  @Input() name: string;
  @Input() placeholder: string;
  @Input() variants: string[];

  constructor() { }

  ngOnInit(): void {
  }
}
