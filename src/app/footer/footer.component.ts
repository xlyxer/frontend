import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  scrollTo(id: string) {
    let el = document.getElementById(id);
    if(el != null) {
      el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
  }
}
